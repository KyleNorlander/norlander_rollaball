﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public Text countText;
    public Text winText;

    private Rigidbody rb;
    private int count;

    void Start()
    {   //Set up reference to rigid body
        rb = GetComponent<Rigidbody>();
        count = 0;
        winText.text = "";
        SetCountText();
    }

    void FixedUpdate()
    {   // Get input on each axis, move the ball
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        //Apply movement to ball

        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {   //Adds collision for Player object and pickups 
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
            //increases score by 1
        }
    }

    internal void movement(Vector3 vector3)
    {

    }

    void SetCountText()
    {   //Creates win text
        countText.text = "Count: " + count.ToString();
        if (count >= 16)
        {
            winText.text = "You Win!";
        }   //If player has 8 or more pickups, win text is displayed
    }
}