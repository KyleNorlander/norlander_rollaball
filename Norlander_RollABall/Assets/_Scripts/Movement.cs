﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{

    public float movementSpeed; //creates movement and jump
    public float rotationSpeed;
    public float rotX;
    public float rotY;
    public float rotZ;

    
    public float stepTimer;
    public float setStepTimer;

    // Used for initialization
    void Start()
    {
        stepTimer = setStepTimer;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetButtonDown("Jump")) // adds jump
        {
            if (transform.position.y <= 1.05f)
            {
                GetComponent<Rigidbody>().AddForce(Vector3.up * 200);
                if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey("w"))
                {
                    transform.position += transform.TransformDirection(Vector3.forward) * Time.deltaTime * movementSpeed * 2.5f;// adds movement
                    stepTimer -= 1 * Time.deltaTime * 2.5f;
                }
                else if (Input.GetKey("w") && !Input.GetKey(KeyCode.LeftShift))
                {
                    transform.position += transform.TransformDirection(Vector3.forward) * Time.deltaTime * movementSpeed;
                    stepTimer -= 1 * Time.deltaTime;
                }
                else if (Input.GetKey("s"))
                {
                    transform.position -= transform.TransformDirection(Vector3.forward) * Time.deltaTime * movementSpeed;
                    stepTimer -= 1 * Time.deltaTime;
                }

                if (Input.GetKey("a") && !Input.GetKey("d"))
                {
                    transform.position += transform.TransformDirection(Vector3.left) * Time.deltaTime * movementSpeed;
                    if (!Input.GetKey("w") && !Input.GetKey("s"))
                    {
                        stepTimer -= 1 * Time.deltaTime;
                    }
                }
                else if (Input.GetKey("d") && !Input.GetKey("a"))
                {
                    transform.position -= transform.TransformDirection(Vector3.left) * Time.deltaTime * movementSpeed;
                    if (!Input.GetKey("w") && !Input.GetKey("s"))
                    {
                        stepTimer -= 1 * Time.deltaTime;
                    }
                }
            }
        }
    }
}