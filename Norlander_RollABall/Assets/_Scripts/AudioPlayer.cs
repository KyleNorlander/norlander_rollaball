﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Audio : MonoBehaviour //creates audio class
{
    public AudioClip clip; 

    void Start() //starts audio
    {
        AudioSource.PlayClipAtPoint(clip, new Vector3(0, 0, 0)); //plays my song at spawn
    }
}